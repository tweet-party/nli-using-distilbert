# NLI using DistilBERT

NLI (natural language inferencing) is a classical NLP problem that involves taking two sentences (the premise and the hypothesis), and deciding how they are related: whether the premise entails the hypothesis, contradicts it, or neither.

On this GitLab, you will find:
- *tp8_subject* which provides a detailled description of the NLI problem we want to tackle.
- *tp8_experimentation* where we experiment with models and functions from the *Huggingface* library.
- *tp8_submission* our solution which is built on top of a *DistilBERT* model. **--> work in progress <--**

# Stanford NLI (SNLI) corpus

In this labwork, we use the Stanford NLI (SNLI) corpus ( https://nlp.stanford.edu/projects/snli/ ), available in the Datasets library by Huggingface.
