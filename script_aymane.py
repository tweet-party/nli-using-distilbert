import torch
from torch.utils.data import DataLoader
from transformers import DistilBertTokenizerFast, DistilBertForSequenceClassification, AdamW, logging
import datasets
from tqdm import tqdm
import numpy as np

from datasets import load_dataset
snli = load_dataset("snli")

#Removing sentence pairs with no label (-1)
snli = snli.filter(lambda example: example['label'] != -1)
#snli['train']['hypothesis']

tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')

def encode(examples):
    return tokenizer(examples['hypothesis'], examples['premise'], truncation=True, padding='max_length')

snli = snli.map(encode, batched=True)
snli = snli.map(lambda examples: {'labels': examples['label']}, batched=True)

snli.set_format(type='torch', columns=['input_ids', 'attention_mask', 'labels'])

trainloader = torch.utils.data.DataLoader(snli['train'], batch_size=20)
validloader = torch.utils.data.DataLoader(snli['validation'], batch_size=20)
testloader = torch.utils.data.DataLoader(snli['test'], batch_size=20)

print(len(trainloader))
print(len(validloader))
print(len(testloader))

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

model = DistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased', num_labels=3)
model.to(device)
model.train()
optim = AdamW(model.parameters(), lr=5e-5)

epochs = 5
for epoch in range(epochs):
    all_losses = []

    for batch in tqdm(trainloader, total=len(trainloader), desc="Epoch: {}/{}".format(epoch+1, epochs)):
        optim.zero_grad()
        input_ids = batch['input_ids'].to(device)
        attention_mask = batch['attention_mask'].to(device)
        labels = batch['labels'].to(device)
        outputs = model(input_ids, attention_mask=attention_mask, labels=labels)
        loss = outputs[0]
        loss.backward()
        optim.step()
        all_losses.append(loss.item())
        
    print("\nMean loss: {:<.4f}".format(np.mean(all_losses)))


model.eval()
with torch.no_grad():
    eval_preds = []
    eval_labels = []

    for batch in tqdm(validloader, total=len(validloader)):
        input_ids = batch['input_ids'].to(device)
        attention_mask = batch['attention_mask'].to(device)
        labels = batch['labels'].to(device)
        preds = model(input_ids, attention_mask=attention_mask, labels=labels)
        preds = preds[1].argmax(dim=-1)
        eval_preds.append(preds.cpu().numpy())
        eval_labels.append(batch['labels'].cpu().numpy())

print("\nValidation accuracy: {:6.2f}".format(round(100 * (np.concatenate(eval_labels) == np.concatenate(eval_preds)).mean()), 2))
